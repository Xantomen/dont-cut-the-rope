﻿using UnityEngine;
using System.Collections;

public class ChangeGoalText : MonoBehaviour {

    public Sprite Number0;
    public Sprite Number1;
    public Sprite Number2;
    public Sprite Number3;
    public Sprite Number4;
    public Sprite Number5;

    private int numberOfBallsInBasket = 0;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        switch(numberOfBallsInBasket)
        {
            case 0:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number0;
                break;
            case 1:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number1;
                break;
            case 2:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number2;
                break;
            case 3:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number3;
                break;
            case 4:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number4;
                break;
            case 5:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number5;
                break;
            default:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = Number0;
                break;

        }
    }

    void increaseBallsInBasket()
    {
        numberOfBallsInBasket += 1;
    }

    void decreaseBallsInBasket()
    {
        numberOfBallsInBasket -= 1;
    }
}
