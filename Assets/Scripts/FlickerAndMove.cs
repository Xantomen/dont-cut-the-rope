﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlickerAndMove : MonoBehaviour {

    public float flickerSpeedVisibleMin = 0.0f;
    public float flickerSpeedVisibleMax = 0.8f;
    private float flickerSpeedVisible = 0.1f;

    public float flickerSpeedHiddenMin = 0.0f;
    public float flickerSpeedHiddenMax = 0.8f;
    private float flickerSpeedHidden = 0.1f;

    private float actualTimeToFlicker;

    private bool isItVisible = false;
    
    public List<Sprite> possibleSpritesList;

    // Use this for initialization
    void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {

        if(actualTimeToFlicker < Time.time)
        {
            if (isItVisible)
            {
                this.GetComponent<SpriteRenderer>().enabled = false;
                flickerSpeedHidden = Random.RandomRange(flickerSpeedHiddenMin, flickerSpeedHiddenMax);
                actualTimeToFlicker = Time.time + flickerSpeedHidden;
                
            }
            else
            {
 
                if(possibleSpritesList.Count > 0)
                {
                    this.GetComponent<SpriteRenderer>().sprite = possibleSpritesList[Random.Range(0, possibleSpritesList.Count)];
                }
                this.GetComponent<SpriteRenderer>().enabled = true;
                flickerSpeedVisible = Random.RandomRange(flickerSpeedVisibleMin, flickerSpeedVisibleMax);
                actualTimeToFlicker = Time.time + flickerSpeedVisible;
            }

            isItVisible = !isItVisible;
        }
        
    }

   
}
