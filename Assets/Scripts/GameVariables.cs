﻿using UnityEngine;
using System.Collections;

public class GameVariables : MonoBehaviour {

	private bool isLevelLoaded = true;

	private bool isRopeCut = false;
	private bool isFloorTouched = false;
	private bool isGoalTouched = false;
	private bool isRestartCalled = false;
	public float timeToRestart = 2.0f;
	private float actualTimeToRestart;
	private bool pressedRestart = false;

    public Sprite StarComplete;
    public Sprite NoStar;

    public int numberOfBallsInBasket = 0;
    public int numberOfBallsRequiredToWin = 0;
    public int numberOfStartingBallsTotal = 0;
    private int numberOfActualBallsInGame;
    public bool isLevelWinnable;

    public bool threeStarNeedRopeNoCut = true;
    public int numberBallsThreeStars = 0;

    public bool isLevelWon = false;

	public int sceneNumber = 0;

    private string typeOfPlatform = "Android";
    private bool isAlwaysBackthrow = false;

    public Sprite backthrowSprite;
    public Sprite tipthrowSprite;

    // Use this for initialization
    void Start () {

        numberOfActualBallsInGame = numberOfStartingBallsTotal;

        if (Application.platform == RuntimePlatform.Android)
            typeOfPlatform = "Android";
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
            typeOfPlatform = "iOS";
    }
	
	// Update is called once per frame
	void Update () {
	
		if(isLevelLoaded)
		{
            
            if(isLevelWinnable)
            {
                if(numberOfBallsInBasket >= numberOfBallsRequiredToWin)
                {
                    GameObject.FindGameObjectWithTag("Star1").GetComponent<SpriteRenderer>().sprite = StarComplete;
                    GameObject.FindGameObjectWithTag("Continue").GetComponent<SpriteRenderer>().enabled = true;
                    GameObject.FindGameObjectWithTag("Continue").GetComponent<BoxCollider2D>().enabled = true;
                    GameObject.FindGameObjectWithTag("ContinueText").GetComponent<SpriteRenderer>().enabled = true;


                    isLevelWon = true;

                }

                if (numberOfBallsInBasket == numberBallsThreeStars)
                {
     
                    GameObject.FindGameObjectWithTag("Star2").GetComponent<SpriteRenderer>().sprite = StarComplete;


                }
                else
                {
                    GameObject.FindGameObjectWithTag("Star2").GetComponent<SpriteRenderer>().sprite = NoStar;
                }

                if (numberOfBallsInBasket == numberBallsThreeStars && (isRopeCut == false || !threeStarNeedRopeNoCut))
                {

                    GameObject.FindGameObjectWithTag("Star3").GetComponent<SpriteRenderer>().sprite = StarComplete;
                }
                else
                {
                    GameObject.FindGameObjectWithTag("Star3").GetComponent<SpriteRenderer>().sprite = NoStar;
                }


                if (numberOfActualBallsInGame < numberOfBallsRequiredToWin)
                {
                    isRestartCalled = true;
                }
            }

			/*if(isRestartCalled == false && isRopeCut == true)
			{
				actualTimeToRestart = Time.time + timeToRestart;

				isRestartCalled = true;

			}*/
			
			if(isRestartCalled == true && actualTimeToRestart < Time.time)
			{
				resetGame();
			}
			
			if(pressedRestart == true || isFloorTouched == true)
			{
				resetGame();
			}
			
			if (Input.GetKeyDown ("r"))
			{
				pressedRestart = true;
			}

			if (Input.GetKeyDown (KeyCode.Escape))
			{
				goToLevelSelectionScreen();
			}

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                string targetTag = "";

                if (hit)
                {
                    targetTag = hit.collider.gameObject.transform.tag;
                }
                if (targetTag.Contains("Restart"))
                {
                    pressedRestart = true;
                }
                if (targetTag.Contains("Escape"))
                {
                    goToLevelSelectionScreen();
                }
                if (targetTag.Contains("Continue"))
                {
                    goToLevelSelectionScreen();
                }
                if (targetTag.Contains("KnifeModeToggle"))
                {
                    isAlwaysBackthrow = !isAlwaysBackthrow;

                    if(isAlwaysBackthrow)
                    {
                        hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite = backthrowSprite;
                    }
                    else
                    {
                        hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite = tipthrowSprite;
                    }

                    GameObject.FindGameObjectWithTag("ProjectileSpawn").SendMessage("setIsAlwaysBackthrow", isAlwaysBackthrow);
                }
                


            }
            if (Input.GetMouseButtonDown(1))
            {

                isAlwaysBackthrow = !isAlwaysBackthrow;

                if (isAlwaysBackthrow)
                {
                    GameObject.FindGameObjectWithTag("KnifeModeToggle").GetComponent<SpriteRenderer>().sprite = backthrowSprite;
                }
                else
                {
                    GameObject.FindGameObjectWithTag("KnifeModeToggle").GetComponent<SpriteRenderer>().sprite = tipthrowSprite;
                }

                GameObject.FindGameObjectWithTag("ProjectileSpawn").SendMessage("setIsAlwaysBackthrow", isAlwaysBackthrow);
            }

        }

			
	}

	void resetGame()
	{
        UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel"+sceneNumber);
	}

	void goToLevelSelectionScreen()
	{
        UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel0");
	}

	void ropeIsCut()
	{
		isRopeCut = true;
	}

	void floorIsTouched()
	{
		isFloorTouched = true;
	}

	void goalIsTouched()
	{
		isGoalTouched = true;

        UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelectionScreen");
        
	}

    void increaseBallsInBasket()
    {
        numberOfBallsInBasket += 1;
    }

    void decreaseBallsInBasket()
    {
        numberOfBallsInBasket -= 1;
    }

    void decreaseActualBallsInGame()
    {
        numberOfActualBallsInGame -= 1;
    }

}
