﻿using UnityEngine;
using System.Collections;

public class GoToLevel : MonoBehaviour {

	public int sceneNumber;
	private bool isFloorTouched = false;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("1"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 1);
        }
        if (Input.GetKeyDown("2"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 2);
        }
        if (Input.GetKeyDown("3"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 3);
        }
        if (Input.GetKeyDown("4"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 4);
        }
        if (Input.GetKeyDown("5"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 5);
        }
        if (Input.GetKeyDown("6"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 6);
        }
        if (Input.GetKeyDown("7"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 7);
        }
        if (Input.GetKeyDown("8"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel" + 8);
        }

    }


	void goToLevel()
	{
		Application.LoadLevel("SceneLevel"+sceneNumber);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Ball")
		{
			
			if(isFloorTouched == false)
			{
				goToLevel();
				isFloorTouched = true;
				
			}
		}
		
	}

}
