﻿using UnityEngine;
using System.Collections;

public class HideIfNotOnPC : MonoBehaviour {

    private string typeOfPlatform = "PC";
    public Sprite PC_Sprite;
    public Sprite Mobile_Sprite;

    // Use this for initialization
    void Start () {

        print(Application.platform);
        
        if (Application.platform == RuntimePlatform.Android)
            typeOfPlatform = "Android";
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
            typeOfPlatform = "iOS";

        print(typeOfPlatform);

        if(typeOfPlatform == "PC")
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = PC_Sprite;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = Mobile_Sprite;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
