﻿using UnityEngine;
using System.Collections;

public class ListenerWhenBallsInBasket : MonoBehaviour {



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Ball")
        {
            GameObject.FindGameObjectWithTag("GameVariables").SendMessage("increaseBallsInBasket");
            GameObject.FindGameObjectWithTag("GoalCounter").SendMessage("increaseBallsInBasket");
        }
        
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Ball")
        {
            GameObject.FindGameObjectWithTag("GameVariables").SendMessage("decreaseBallsInBasket");
            GameObject.FindGameObjectWithTag("GoalCounter").SendMessage("decreaseBallsInBasket");
        }

    }


}
