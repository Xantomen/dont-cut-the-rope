﻿using UnityEngine;
using System.Collections;

public class LoseIfTouch : MonoBehaviour {

	private bool isFloorTouched = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Ball")
		{

			if(isFloorTouched == false)
			{
				GameObject.FindGameObjectWithTag("GameVariables").SendMessage("floorIsTouched");
				isFloorTouched = true;
				
			}
		}
		
	}


}
