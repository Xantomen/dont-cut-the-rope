﻿using UnityEngine;
using System.Collections;

public class MainMenuBehaviour : MonoBehaviour {

	private bool isLevelLoaded = true;

	private bool isRopeCut = false;

	private bool isRestartCalled = false;
	public float timeToRestart = 2.0f;
	private float actualTimeToRestart;
	private bool pressedRestart = false;

	private float startTime;
	public float limitTimeToWait = 10;
	private float currentTimeToWaitGoal;

	private bool timeInitialised = false;

	// Use this for initialization
	void Start () {
	
		startTime = Time.time;
		currentTimeToWaitGoal = startTime + limitTimeToWait;
		timeInitialised = true;


	}
	
	// Update is called once per frame
	void Update () {
	
		if(isLevelLoaded)
		{
			if(isRestartCalled == false && isRopeCut == true)
			{
				actualTimeToRestart = Time.time + timeToRestart;

				isRestartCalled = true;

			}
			
			if(isRestartCalled == true && actualTimeToRestart < Time.time)
			{
				goToLevelSelectionScreen();
			}

			
			if (Input.GetKeyDown ("r"))
			{
				pressedRestart = true;
			}

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (currentTimeToWaitGoal < Time.time && timeInitialised == true)
			{
		
				goToLevelSelectionScreen();
			}

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                string targetTag = "";

                if (hit)
                {
                    targetTag = hit.collider.gameObject.transform.tag;
                }
                if (targetTag.Contains("Level"))
                {

                    print(targetTag);
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Scene" + targetTag);
                }
                if (targetTag.Contains("ExitGame"))
                {
                    print(targetTag);
                    Application.Quit();
                }
            }
        }

			
	}

	void goToLevelSelectionScreen()
	{

        UnityEngine.SceneManagement.SceneManager.LoadScene("SceneLevel0");
	}

	void goBackToMainMenu()
	{
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
	}

	void ropeIsCut()
	{
		isRopeCut = true;
	}


}
