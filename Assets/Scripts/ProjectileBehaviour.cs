﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectileBehaviour : MonoBehaviour {

	public bool isLaunched = false;
	private Vector3 initialMousePosition;
	private bool isBackthrow = true;
    private bool isMoving = false;

    public float initialProjectileSpeed = 1000.0f;
	private float projectileSpeed;
    public float projectileAcceleration = 1000.0f;
    public float minProjectileAcceleration = 100f;

    public float maxProjectileVelocity = 60f;
    //Tension is amount to divide for the shorter an arrow is
    public float tensionFactor = 10.0f;

	private bool hasCollided = false;
	private bool isStuck = false;
	private bool isRopeCut = false;

	public GameObject arrowIndicator;
	private GameObject temp_ArrowIndicator;

	private bool isArrowCreated = false;

	public AudioClip throw_Knife_Sound;
	public AudioClip stab_Wood_Knife_Sound;

	public List<AudioClip> knife_Hit_Sound_List = new List<AudioClip>();

    private bool isAlwaysBackthrow = false;


    // Use this for initialization
    void Start () {
	
		initialMousePosition = this.transform.position;
        projectileSpeed = initialProjectileSpeed;
        projectileAcceleration = initialProjectileSpeed;
        
    }
	
	// Update is called once per frame
	void Update () {
	
		if(isArrowCreated == false)
		{
			temp_ArrowIndicator = Instantiate(arrowIndicator,this.transform.position+this.transform.up*2.5f,this.transform.rotation) as GameObject;

			isArrowCreated = true;
		}

        if(Input.GetMouseButtonDown(1) && isLaunched == false)
        {
            GameObject.Destroy(this.gameObject);
            GameObject.Destroy(temp_ArrowIndicator);
        }

		if((Input.GetMouseButtonUp(0)) && isLaunched == false)
		{
			isLaunched = true;

			this.gameObject.GetComponent<AudioSource>().Stop();
			this.gameObject.GetComponent<AudioSource>().clip = throw_Knife_Sound;
			this.gameObject.GetComponent<AudioSource>().Play();

			GameObject.Destroy (temp_ArrowIndicator);
		}

		if(isLaunched == false)
		{
			if(Input.GetMouseButton(0))
			{
				isBackthrow = false;
			}
			if(Input.GetMouseButton(1))
			{
				isBackthrow = true;
			}

			lookAtMouse();
		}

		if(isLaunched == true && !hasCollided && !isStuck && isMoving == false)
		{

            Vector3 mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

            float temp_dist = (initialMousePosition - mousePosition).magnitude;


            if (temp_dist > 30.0f)
            {
                temp_dist = 30.0f;
            }
            else if (temp_dist < 27.0f)
            {
                temp_dist = 27.0f;
            }

            projectileAcceleration = initialProjectileSpeed * tensionFactor * ((temp_dist - 27) / 3);

            if (projectileAcceleration < minProjectileAcceleration)
            {
                projectileAcceleration = minProjectileAcceleration;
            }
               

            //print("DIST:" + temp_dist + "; " + "DIST FACTOR:" + ((temp_dist - 27) / 3) + "; " + "ACCEL:" + projectileAcceleration);

            moveProjectileForwards();

            isMoving = true;

        }
        
        if(isMoving == true && !hasCollided && !isStuck)
        {
            moveProjectileForwards();
        }


    }

	/*void createAndOrientProjectile(Vector2 mousePosition)
	{
		initialMousePosition = mousePosition;
		GameObject temp_projectile = Instantiate(projectile,initialMousePosition,Quaternion.identity) as GameObject;
	}*/
	
	void lookAtMouse()
	{
		Vector3 mousePosition = Input.mousePosition;           
		mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

		//transform.LookAt(new Vector3(mousePosition.x, mousePosition.y, transform.position.x));
		Quaternion rot = Quaternion.LookRotation(transform.position - mousePosition, Vector3.forward );
		transform.rotation = rot;  
		if(!isBackthrow && !isAlwaysBackthrow)
		{
			transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z);
			if(temp_ArrowIndicator)
			{
				float temp_dist = (initialMousePosition - mousePosition).magnitude;
				temp_ArrowIndicator.transform.localScale = new Vector2(0.8f,temp_dist/30*temp_dist/30);
				temp_ArrowIndicator.transform.eulerAngles = transform.eulerAngles;
				temp_ArrowIndicator.transform.position = this.transform.position+this.transform.up+this.transform.up*temp_dist/30*temp_dist/30;
			}

		}
		else
		{
			transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z-180);
			if(temp_ArrowIndicator)
			{
				float temp_dist = (initialMousePosition - mousePosition).magnitude;
				temp_ArrowIndicator.transform.localScale = new Vector2(0.8f,temp_dist/30*temp_dist/30);
				temp_ArrowIndicator.transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z-180);
				temp_ArrowIndicator.transform.position = this.transform.position-this.transform.up-this.transform.up*temp_dist/30*temp_dist/30;

			}
		}



	}

	void moveProjectileForwards()
	{
        if(!isMoving)
        {
            if (!isBackthrow && !isAlwaysBackthrow)
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * projectileSpeed);               

            }
            else
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(-transform.up * projectileSpeed);
            }
        }
        else
        {
            if (!isBackthrow && !isAlwaysBackthrow)
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * projectileAcceleration);
            }
            else
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(-transform.up * projectileAcceleration);
            }

            Vector2 velocity = this.gameObject.GetComponent<Rigidbody2D>().velocity;

            if (velocity.x > maxProjectileVelocity)
            {
                velocity.x = maxProjectileVelocity;
            }
            else if (velocity.x < -maxProjectileVelocity)
            {
                velocity.x = -maxProjectileVelocity;
            }
            if (velocity.y > maxProjectileVelocity)
            {
                velocity.y = maxProjectileVelocity;
            }
            else if (velocity.y < -maxProjectileVelocity)
            {
                velocity.y = -maxProjectileVelocity;
            }

            this.gameObject.GetComponent<Rigidbody2D>().velocity = velocity;
        }
        

    }

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(hasCollided == false && (collision.gameObject.tag == "Obstacle" || collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Projectile" || collision.gameObject.tag == "Ceiling" || collision.gameObject.tag == "Goal" || collision.gameObject.tag == "NonPierceObstacle"))
		{
			hasCollided = true;

			this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;

			this.gameObject.GetComponent<AudioSource>().Stop();
			this.gameObject.GetComponent<AudioSource>().clip = knife_Hit_Sound_List[Random.Range(0,knife_Hit_Sound_List.Count-1)];
			this.gameObject.GetComponent<AudioSource>().Play();

            /*if (collision.gameObject.tag == "ChainRope" || collision.gameObject.tag == "NonPierceObstacle")
            {
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-this.gameObject.GetComponent<Rigidbody2D>().velocity.x / 8, this.gameObject.GetComponent<Rigidbody2D>().velocity.y / 4);
                this.gameObject.GetComponent<Rigidbody2D>().AddTorque(800f);

            }*/
        }

        if (hasCollided == false && collision.gameObject.tag == "FragileRope")
        {
            hasCollided = true;

            if (isStuck == false)
            {
                GameObject.Destroy(collision.gameObject);

                if (isRopeCut == false)
                {
                    GameObject.FindGameObjectWithTag("GameVariables").SendMessage("ropeIsCut");
                    isRopeCut = true;

                }

            }
        }

    }

	void OnTriggerEnter2D(Collider2D collider)
	{


		if(isStuck == false && (collider.gameObject.tag == "Obstacle" || collider.gameObject.tag == "Ball"))
		{
			isStuck = true;
			
			this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
			
			this.gameObject.GetComponent<HingeJoint2D>().connectedBody = collider.gameObject.GetComponent<Rigidbody2D>();
			this.gameObject.GetComponent<HingeJoint2D>().enabled = true;

			this.gameObject.GetComponent<AudioSource>().Stop();
			this.gameObject.GetComponent<AudioSource>().clip = stab_Wood_Knife_Sound;
			this.gameObject.GetComponent<AudioSource>().Play();

		}

		if(isStuck == false && collider.gameObject.tag == "Rope")
		{
			GameObject.Destroy(collider.gameObject);

			if(isRopeCut == false)
			{
				GameObject.FindGameObjectWithTag("GameVariables").SendMessage("ropeIsCut");
				isRopeCut = true;

			}

		}

        if (isStuck == false && collider.gameObject.tag == "ChainRope")
        {
            hasCollided = true;

            this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;

            this.gameObject.GetComponent<AudioSource>().Stop();
            this.gameObject.GetComponent<AudioSource>().clip = knife_Hit_Sound_List[Random.Range(0, knife_Hit_Sound_List.Count - 1)];
            this.gameObject.GetComponent<AudioSource>().Play();

            
            /*this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-this.gameObject.GetComponent<Rigidbody2D>().velocity.x / 8, this.gameObject.GetComponent<Rigidbody2D>().velocity.y / 4);
            this.gameObject.GetComponent<Rigidbody2D>().AddTorque(800f);*/

        }


    }

    void setIsAlwaysBackthrow(bool value)
    {
        isAlwaysBackthrow = value;
    }

}
