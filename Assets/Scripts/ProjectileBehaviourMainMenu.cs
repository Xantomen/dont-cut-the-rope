﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectileBehaviourMainMenu : MonoBehaviour {

	private bool isLaunched = false;
	private Vector3 initialMousePosition;
	private bool isBackthrow = false;

	public float projectileSpeed = 10.0f;

	private bool hasCollided = false;
	private bool isStuck = false;
	private bool isRopeCut = false;

	public GameObject arrowIndicator;
	private GameObject temp_ArrowIndicator;

	private bool isArrowCreated = false;

	public AudioClip throw_Knife_Sound;
	public AudioClip stab_Wood_Knife_Sound;
	
	public List<AudioClip> knife_Hit_Sound_List = new List<AudioClip>(); 

	// Use this for initialization
	void Start () {
	
		initialMousePosition = this.transform.position;


	}
	
	// Update is called once per frame
	void Update () {
	
		if(isArrowCreated == false)
		{
			temp_ArrowIndicator = Instantiate(arrowIndicator,this.transform.position+this.transform.up*2.5f,this.transform.rotation) as GameObject;

			isArrowCreated = true;

		}

		if((Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))  && isLaunched == false)
		{
			isLaunched = true;

			this.gameObject.GetComponent<AudioSource>().Stop();
			this.gameObject.GetComponent<AudioSource>().clip = throw_Knife_Sound;
			this.gameObject.GetComponent<AudioSource>().Play();

			GameObject.Destroy (temp_ArrowIndicator);
		}

		if(isLaunched == false)
		{
			if(Input.GetMouseButton(0))
			{
				isBackthrow = false;
			}
			if(Input.GetMouseButton(1))
			{
				isBackthrow = true;
			}

			lookAtMouse();
		}

		if(isLaunched == true && !hasCollided && !isStuck)
		{

			moveProjectileForwards();

		}


	}

	/*void createAndOrientProjectile(Vector2 mousePosition)
	{
		initialMousePosition = mousePosition;
		GameObject temp_projectile = Instantiate(projectile,initialMousePosition,Quaternion.identity) as GameObject;
	}*/
	
	void lookAtMouse()
	{
		Vector3 mousePosition = Input.mousePosition;           
		mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

		//transform.LookAt(new Vector3(mousePosition.x, mousePosition.y, transform.position.x));
		Quaternion rot = Quaternion.LookRotation(transform.position - mousePosition, Vector3.forward );
		transform.rotation = rot;  
		if(!isBackthrow)
		{
			transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z);
			if(temp_ArrowIndicator)
			{
				float temp_dist = (initialMousePosition - mousePosition).magnitude;
				temp_ArrowIndicator.transform.localScale = new Vector2(0.8f,temp_dist/30*temp_dist/30);
				temp_ArrowIndicator.transform.eulerAngles = transform.eulerAngles;
				temp_ArrowIndicator.transform.position = this.transform.position+this.transform.up+this.transform.up*temp_dist/30*temp_dist/30;
			}

		}
		else
		{
			/*transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z);
			if(temp_ArrowIndicator)
			{
				float temp_dist = (initialMousePosition - mousePosition).magnitude;
				temp_ArrowIndicator.transform.localScale = new Vector2(0.8f,temp_dist/30*temp_dist/30);
				temp_ArrowIndicator.transform.eulerAngles = transform.eulerAngles;
				temp_ArrowIndicator.transform.position = this.transform.position+this.transform.up+this.transform.up*temp_dist/30*temp_dist/30;
			}*/
			transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z-180);
			if(temp_ArrowIndicator)
			{
				float temp_dist = (initialMousePosition - mousePosition).magnitude;
				temp_ArrowIndicator.transform.localScale = new Vector2(0.8f,temp_dist/30*temp_dist/30);
				temp_ArrowIndicator.transform.eulerAngles = new Vector3(0, 0,transform.eulerAngles.z-180);
				temp_ArrowIndicator.transform.position = this.transform.position-this.transform.up-this.transform.up*temp_dist/30*temp_dist/30;

			}
		}



	}

	void moveProjectileForwards()
	{
		if(!isBackthrow)
		{
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * projectileSpeed);
		}
		else
		{
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(-transform.up * projectileSpeed);
		}

	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(hasCollided == false && (collision.gameObject.tag == "Obstacle" || collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Projectile" || collision.gameObject.tag == "Ceiling" || collision.gameObject.tag == "Goal"))
		{
			hasCollided = true;

			this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;

			this.gameObject.GetComponent<AudioSource>().Stop();
			this.gameObject.GetComponent<AudioSource>().clip = knife_Hit_Sound_List[Random.Range(0,knife_Hit_Sound_List.Count-1)];
			this.gameObject.GetComponent<AudioSource>().Play(); 
		}


	}

	void OnTriggerEnter2D(Collider2D collider)
	{


		if(isStuck == false && (collider.gameObject.tag == "Obstacle" || collider.gameObject.tag == "Ball"))
		{
			isStuck = true;

			//print("STUCK!");
			
			this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
			
			this.gameObject.GetComponent<HingeJoint2D>().connectedBody = collider.gameObject.GetComponent<Rigidbody2D>();
			this.gameObject.GetComponent<HingeJoint2D>().enabled = true;

			this.gameObject.GetComponent<AudioSource>().Stop();
			this.gameObject.GetComponent<AudioSource>().clip = stab_Wood_Knife_Sound;
			this.gameObject.GetComponent<AudioSource>().Play();

		}

		if(isStuck == false && collider.gameObject.tag == "Rope")
		{
			GameObject.Destroy(collider.gameObject);

			if(isRopeCut == false)
			{
				GameObject.FindGameObjectWithTag("GameVariables").SendMessage("ropeIsCut");
				isRopeCut = true;

			}

		}


	}

}
