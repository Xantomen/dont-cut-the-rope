﻿using UnityEngine;
using System.Collections;

public class ProjectileSpawn : MonoBehaviour {

    public GameObject AllContainer;
	public GameObject projectile;

	private Vector2 initialMousePosition;
	private Vector2 destinationMousePosition;

	private Quaternion actualMouseRotation;

	private bool isBusyCreating = false;

	private bool spawnKnife = true;
    
    private bool isAlwaysBackthrow = false;
    

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
        
            if (Input.GetMouseButtonDown(0) && isBusyCreating == false)
            {

                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                string targetTag = "";

                if (hit)
                {
                    targetTag = hit.collider.gameObject.transform.tag;
                }
                if (targetTag == "KnifeAllowedArea")
                {

                    if (spawnKnife)
                    {
                        initialMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        GameObject temp_projectile = Instantiate(projectile.gameObject, initialMousePosition, Quaternion.identity) as GameObject;

                        GameObject[] all_Projectiles = GameObject.FindGameObjectsWithTag("Projectile");

                        for (var i = 0; i < all_Projectiles.Length; i++)
                        {
                            if(all_Projectiles[i].GetComponent<ProjectileBehaviour>().isLaunched == false)
                            {
                                all_Projectiles[i].SendMessage("setIsAlwaysBackthrow", isAlwaysBackthrow);
                            }
                           
                        }
                    }
                    isBusyCreating = true;

                }

            }
            if (Input.GetMouseButtonUp(0) && isBusyCreating == true)
            {
                isBusyCreating = false;
            }
            
        
		
    }

    void setIsAlwaysBackthrow(bool value)
    {
        isAlwaysBackthrow = value;
    }


}
