﻿using UnityEngine;
using System.Collections;

public class ProjectileSpawnMainMenu : MonoBehaviour {

	public GameObject projectile;

	private Vector2 initialMousePosition;
	private Vector2 destinationMousePosition;

	private Quaternion actualMouseRotation;

	private bool isBusyCreating = false;

	private bool spawnKnife = true;

	// Use this for initialization
	void Start () {
	
		spawnKnife = false;
	}
	
	// Update is called once per frame
	void Update () {
	

			if((Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) && isBusyCreating == false)
			{
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                string targetTag = "";

                if (hit)
                {
                    targetTag = hit.collider.gameObject.transform.tag;
                }
                if (targetTag == "KnifeAllowedArea")
                {

                    if (spawnKnife)
                    {
                        initialMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        GameObject temp_projectile = Instantiate(projectile, initialMousePosition, Quaternion.identity) as GameObject;
                    }
                    spawnKnife = true;
                    isBusyCreating = true;

                }
            }

        
        if ((Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(1)) && isBusyCreating == true)
			{
				isBusyCreating = false;
			}



	}
	

}
