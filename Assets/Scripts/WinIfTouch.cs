﻿using UnityEngine;
using System.Collections;

public class WinIfTouch : MonoBehaviour {

	private bool isGoalTouched = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Ball")
		{
			
			if(isGoalTouched == false)
			{
				GameObject.FindGameObjectWithTag("GameVariables").SendMessage("goalIsTouched");
				isGoalTouched = true;
				
			}
		}
		
	}
}
